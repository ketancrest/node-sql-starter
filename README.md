# Node SQL Starter

NodeJS, ExpressJS, PostgreSQL - Boilerplate

## Installation

1. Clone the repo
	
	```
	$ git clone https://bitbucket.org/ketancrest/node-sql-starter.git
	```

2. Install NPM packages
	
	```
	$ cd node-sql-starter && npm install
	```

3. Copy `.env.example` to `.env`
	
	```
	$ cp .env.example .env
	```

4. Run database migration
	
	```
	$ npm run migration
	```

5. Run Server
	
	```
	$ npm start
	```